use super::RawPeripheral;
use sparrow_bitpacker::PackedType;
use core::marker::PhantomData;
use typenum::Unsigned;

use crate::errors::PeripheralResult;

/// Represents a specific word within a RawPeripheral instance, referenced
/// by the `IDX` parameter (which is a [`typenum::Unsigned`]).
///
/// The purpose of this object is it allows downstream code to own the bit
/// of peripheral that they write to. Specifically, the detail around issuing
/// these instances are delegated to the RawPeripheral, and any code that
/// wishes to make use of these need not be concerned with what happens
/// behind the scenes.
///
/// ```
///
/// use sparrow_raw_peripherals::RawPeripheral;
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use sparrow_bitpacker::PackedU16;
/// use typenum::consts::*;
///
/// let mut handler = MemoryHandler::<PackedU16>::new();
/// let p = MockPeripheral::new(handler);
///
/// let zeroth_word = p.to_word::<U0>();
/// let fifth_word = p.to_word::<U5>();
///
/// // We can write through this interface
/// zeroth_word.write(10);
/// fifth_word.write(99);
///
/// assert_eq!(p.read(0).unwrap(), 10);
/// assert_eq!(p.read(5).unwrap(), 99);
/// assert_eq!(p.read(2).unwrap(), 0);
///
/// let second_word = p.to_word::<U2>();
///
/// p.write(2, 123);
/// p.write(5, 1010);
///
/// // We can read too
/// assert_eq!(second_word.read().unwrap(), 123);
/// assert_eq!(fifth_word.read().unwrap(), 1010);
/// ```
/// The `From` trait is also implemented on this struct, so you can create
/// the word instance using `from` and `into`:
/// ```
/// use sparrow_raw_peripherals::{RawPeripheral, RawPeripheralWord};
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use sparrow_bitpacker::PackedU32;
/// use typenum::consts::*;
///
/// let mut handler = MemoryHandler::new();
/// let p = MockPeripheral::new(handler);
///
/// assert_eq!(p.read(1).unwrap(), 0);
/// assert_eq!(p.read(10).unwrap(), 0);
///
/// let first_word = RawPeripheralWord::<_, U1, PackedU32>::from(&p);
/// first_word.write(23);
/// assert_eq!(p.read(1).unwrap(), 23);
///
/// let tenth_word: RawPeripheralWord::<_, U10, PackedU32> = (&p).into();
/// tenth_word.write(765);
/// assert_eq!(p.read(10).unwrap(), 765);
/// ```
pub struct RawPeripheralWord<P, IDX, T>
where
    P: RawPeripheral<T>,
    IDX: Unsigned,
    T: PackedType,
{
    raw_peripheral: P,
    idx_marker: PhantomData<IDX>,
    t_marker: PhantomData<T>,
}

impl<P, IDX, T> RawPeripheralWord<P, IDX, T>
where
    P: RawPeripheral<T>,
    IDX: Unsigned,
    T: PackedType,
{
    /// Creates a new instance that takes ownership of `raw_peripheral`.
    pub fn new(raw_peripheral: P) -> RawPeripheralWord<P, IDX, T> {
        RawPeripheralWord::<P, IDX, T> {
            raw_peripheral,
            idx_marker: PhantomData,
            t_marker: PhantomData,
        }
    }

    /// Writes `value` out.
    pub fn write(&self, value: T::Type) -> PeripheralResult<()> {
        self.raw_peripheral.write(IDX::USIZE, value)
    }

    /// Reads in the value currently on the word.
    pub fn read(&self) -> PeripheralResult<T::Type> {
        self.raw_peripheral.read(IDX::USIZE)
    }
}

impl<P, IDX, T> From<&P> for RawPeripheralWord<P, IDX, T>
where
    P: RawPeripheral<T>,
    IDX: Unsigned,
    T: PackedType,
{
    fn from(raw_peripheral: &P) -> RawPeripheralWord<P, IDX, T> {
        raw_peripheral.to_word::<IDX>()
    }
}
