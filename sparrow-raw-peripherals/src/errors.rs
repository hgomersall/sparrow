use snafu::Snafu;
#[cfg(feature = "std")]
use std::io;
use core::num::TryFromIntError;

#[derive(Debug, Snafu)]
#[snafu(visibility(pub))]
pub enum PeripheralError {
    #[snafu(display("Error converting index to a valid type: {}", source))]
    IndexConversionError { source: TryFromIntError },
    #[snafu(display("Error acquiring the mutex lock"))]
    MutexAcquisitionError,
    #[cfg(feature = "std")]
    #[snafu(display("Error accessing the device file: {}", source))]
    DeviceAccessError { source: io::Error },
    #[cfg(feature = "std")]
    #[snafu(display("Error configuring the device: {}", source))]
    DeviceConfigurationError { source: io::Error },
    #[cfg(feature = "std")]
    #[snafu(display("Error reading from peripheral: {}", source))]
    ReadError { source: io::Error },
    #[cfg(feature = "std")]
    #[snafu(display("Error writing to peripheral: {}", source))]
    WriteError { source: io::Error },
}

pub type PeripheralResult<T, E = PeripheralError> = core::result::Result<T, E>;
