#![cfg_attr(not(feature = "std"), no_std)]

pub mod errors;

#[cfg(feature = "std")]
pub mod mmap;

#[cfg(feature = "std")]
pub mod mock;

pub mod peripheral_word;
pub mod traits;

pub use errors::{PeripheralError, PeripheralResult};
pub use peripheral_word::RawPeripheralWord;
pub use traits::RawPeripheral;
