
use sparrow_bitpacker::Bitfield;
use sparrow_registers::WriteableRegister;

use crate::registers::{control_bitfields, ControlBitfields};

pub mod registers {

    use sparrow_bitpacker::{Bitfield, BoolBitfield, PackedU32, UintBitfield};
    use sparrow_raw_peripherals::RawPeripheral;

    use typenum::consts::*;

    pub type UintBf<Offset, Length, M, D = U0> = UintBitfield<Offset, Length, PackedU32, D, M>;
    pub type BoolBf<Offset, M, D = B0> = BoolBitfield<Offset, PackedU32, D, M>;

    pub type ControlRegister<P> = sparrow_registers::WriteOnlyRegister<P, ControlBitfields, U0, PackedU32>;

    #[derive(Default, Clone, PartialEq, Debug)]
    pub struct ControlBitfields {
        pub go: control_bitfields::Go,
        pub stop: control_bitfields::Stop,
        pub length: control_bitfields::Length,
    }

    impl Bitfield<PackedU32> for ControlBitfields {
        type Error = sparrow_bitpacker::BitfieldError;

        fn pack(&self) -> Result<u32, Self::Error> {

            Ok(self.go.pack()? | self.stop.pack()? | self.length.pack()?)
        }

        fn unpack(raw_val: u32) -> Result<Self, Self::Error> {

            Ok(
                ControlBitfields {
                    go: <control_bitfields::Go>::unpack(raw_val)?,
                    stop: control_bitfields::Stop::unpack(raw_val)?,
                    length: control_bitfields::Length::unpack(raw_val)?,
                }
            )
        }
    }

    pub mod control_bitfields {
        //use std::ops::{Deref, DerefMut};

        use super::{BoolBf, UintBf};
        //use sparrow_bitpacker::{Bitfield, BitfieldError, PackedU32};
        use typenum::consts::*;

        pub type Go = BoolBf<U0, markers::Go>;

        // Alternative method creating a new struct
        //#[derive(Debug, Default, PartialEq, Clone, Copy)]
        //pub struct Go(pub BoolBf<U0, PackedU32>);

        //impl Go {
        //    pub fn new(val: bool) -> Self {
        //        Go(BoolBf::<U0, PackedU32>::new(val))
        //    }
        //}

        //impl Bitfield<PackedU32> for Go {

        //    type Error = BitfieldError;

        //    fn pack(&self) -> Result<u32, Self::Error> {
        //        self.0.pack()
        //    }

        //    fn unpack(raw_val: u32) -> Result<Self, Self::Error> {
        //        Ok(Go(BoolBf::<U0, PackedU32>::unpack(raw_val)?))
        //    }
        //}

        //impl Deref for Go {
        //    type Target = BoolBf<U0, PackedU32>;
        //    fn deref(&self) -> &Self::Target {
        //        &self.0
        //    }
        //}

        //impl DerefMut for Go {
        //    fn deref_mut(&mut self) -> &mut Self::Target {
        //        &mut self.0
        //    }
        //}


        pub type Stop = BoolBf<U1, markers::Stop>;
        pub type Length = UintBf<U2, U10, markers::Length, U55>;

        pub mod markers {
            #[derive(Debug, Copy, PartialEq, Clone)]
            pub struct Go;

            #[derive(Debug, Copy, PartialEq, Clone)]
            pub struct Stop;

            #[derive(Debug, Copy, PartialEq, Clone)]
            pub struct Length;
        }
    }

    /// A register layout encapsulates instances of registers and manages
    /// the underlying raw peripheral.
    /// ```
    /// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
    /// use sparrow_registers::registers::registers;
    ///
    /// let mut handler = MemoryHandler::new();
    /// let p = MockPeripheral::new(&mut handler);
    ///
    /// let kept_p = p.clone();
    /// let mut reg_layout = registers::RegistersLayout::new(p);
    ///
    /// let mut bitfields = ControlBitfields {
    ///     go: control_bitfields::Go::new(true),
    ///     ..Default::default()
    /// };
    ///
    /// bitfields.length.update(10).unwrap();
    ///
    /// reg_layout.control.write(bitfields.clone()).unwrap();
    ///
    /// let stored_val = kept_p.lock_handler().handle_read(0).unwrap();
    ///
    /// assert_eq!(stored_val, bitfields.pack().unwrap());
    /// ```
    pub struct RegistersLayout<P>
    where
        P: RawPeripheral,
    {
        #[allow(dead_code)]
        raw_peripheral: P,
        pub control: ControlRegister<P>,
    }
    impl<P> RegistersLayout<P>
    where
        P: RawPeripheral,
    {
        pub fn new(raw_peripheral: P) -> Self {
            let control = ControlRegister::new(&raw_peripheral);
            RegistersLayout::<P> {
                raw_peripheral,
                control,
            }
        }
    }
}

fn main() {
    use sparrow_raw_peripherals::mock::{MemoryHandler, MockHandler, MockPeripheral};

    let handler = MemoryHandler::new();
    let p = MockPeripheral::new(handler);

    let kept_p = p.clone();
    let mut reg_layout = registers::RegistersLayout::new(p);

    let mut bitfields = ControlBitfields {
        go: control_bitfields::Go::new(true),
        ..Default::default()
    };

    println!("bitfields, showing default vals for all except go:");
    dbg!(&bitfields);

    bitfields.length.update(10).unwrap();

    // Show partial_eq implemented
    dbg!(&bitfields == &bitfields);

    println!("After updating length:");
    dbg!(&bitfields);

    reg_layout.control.write(bitfields.clone()).unwrap();

    let stored_val = kept_p.lock_handler().handle_read(0).unwrap();

    assert_eq!(stored_val, bitfields.pack().unwrap());
}

