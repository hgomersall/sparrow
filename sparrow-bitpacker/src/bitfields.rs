
use crate::{PackedType, PackedU128, PackedU16, PackedU32, PackedU64, PackedU8, TypeLength};
use core::convert::Infallible;
use core::marker::PhantomData;
use core::mem::size_of;
use core::ops::Add;
use num::traits::AsPrimitive;
use num::{NumCast, One, Zero};
use snafu::Snafu;
use typenum::{consts::*, Bit, IsGreaterOrEqual, Len, ToInt};
use typenum::{Cmp, Less, NonZero, Sum, Unsigned};

#[cfg(feature = "std")]
use core::fmt;

const _BYTE_BITS: usize = 8;

const fn _word_size<T>() -> usize {
    size_of::<T>() * _BYTE_BITS
}

#[derive(Debug, PartialEq, Snafu)]
pub enum BitfieldError {
    /// Cannot set bitfield with an out of range value
    OverRange,
    /// The uint is not convertible to a bool (i.e. not 0 or 1).
    InvalidNonBoolConversionFromUintToBool,
    /// The value unpacked to a low, but since the bitfield is const hi, it
    /// should have unpacked to a high.
    InvalidConstHiUnpackedValue,
    /// The type mapped from Infallible. That is, it can never happen.
    Infallible,
}

impl From<Infallible> for BitfieldError {
    fn from(_: Infallible) -> Self {
        BitfieldError::Infallible
    }
}

/// A simple marker trait to act as a universal default.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub struct DefaultNewTypeMarker;

pub type BitfieldResult<T, E = BitfieldError> = core::result::Result<T, E>;

/// Represents a bitfield object that can be packed or unpacked into or from
/// an integer type.
///
/// Implementations might handle multiple discrete bitfields as part of one
/// Bitfield object.
pub trait Bitfield<T>: Sized
where
    T: PackedType + PartialEq,
{
    type Error;

    /// Packs the bitfield value properly aligned into a u32 and returns
    /// the result.
    fn pack(&self) -> Result<T::Type, Self::Error>;

    /// Takes a packed word and tries to unpack it according to its layout
    /// which is specified by the concrete implementation.
    ///
    /// Bits that lie outside of the bitfield are ignored.
    ///
    /// Errors occur if the bitfield specification does not support the value
    /// contained in the respective bits of `raw_val` (e.g. it is out of
    /// range).
    fn unpack(raw_val: T::Type) -> Result<Self, Self::Error>;
}

pub trait BitfieldPack<T>: Sized
where
    T: PackedType + PartialEq
{
    type Error;

    /// Packs the bitfield value properly aligned into a u32 and returns
    /// the result.
    fn pack(&self) -> Result<T::Type, Self::Error>;
}

impl <B, T> BitfieldPack<T> for B
where
    B: Bitfield<T>,
    T: PackedType + PartialEq,
{
    type Error = B::Error;

    fn pack(&self) -> Result<T::Type, Self::Error> {
        <Self as Bitfield<T>>::pack(self)
    }
}

pub trait BitfieldUnpack<T>: Sized
where
    T: PackedType + PartialEq
{
    type Error;

    /// Takes a packed word and tries to unpack it according to its layout
    /// which is specified by the concrete implementation.
    ///
    /// Bits that lie outside of the bitfield are ignored.
    ///
    /// Errors occur if the bitfield specification does not support the value
    /// contained in the respective bits of `raw_val` (e.g. it is out of
    /// range).
    fn unpack(raw_val: T::Type) -> Result<Self, Self::Error>;
}

impl <B, T> BitfieldUnpack<T> for B
where
    B: Bitfield<T>,
    T: PackedType + PartialEq,
{
    type Error = B::Error;

    fn unpack(raw_val: T::Type) -> Result<Self, Self::Error> {
        <Self as Bitfield<T>>::unpack(raw_val)
    }
}

/// Provides the length of the bitfield through the `Length` associated type
/// (as a `typenum`).
#[deprecated(note = "Use BitfieldConfig instead.")]
pub trait BitfieldLength: Sized {
    type Length: Unsigned;
}

pub trait BitfieldConfig {
    type Length: Unsigned;
    type Offset: Unsigned;

    const LENGTH: usize;
    const OFFSET: usize;

    fn length(&self) -> usize {
        Self::LENGTH
    }

    fn offset(&self) -> usize {
        Self::OFFSET
    }
}

// We want to be able to do the following, which would allow downstream
// structs and enums to become Bitfields very simply by implementing AsBitfield.
// The problem is, as it stands this doesn't work because the blanket impl
// of Bitfield conflicts with the specific implementation in UintBitfield,
// BoolBitfield etc. We could work around this with a lower level `RawBitfield`,
// and then have everything implement `AsBitfield`, but there are problems with
// how Error is set on Bitfield in terms of breaking downstream usage. It can
// probably be done by boxing up Error, but this seems a bit clunky.
// We pause implementing this for the moment until we have time to think through
// properly.
//pub trait AsBitfield<P>: TryInto<P::Type> + TryFrom<P::Type>
//where
//    P: PackedType + PartialEq,
//{
//    type AsBitfieldType: Bitfield<P>;
//}
//
//impl <T, P> Bitfield<P> for T
//where
//    T: AsBitfield<P>,
//    P: PackedType + PartialEq,
//{
//    type Error = BitfieldError;
//
//    fn pack(&self) -> Result<<P as PackedType>::Type, Self::Error> {
//        // implement the pack using TryInto<>
//    }
//
//    fn unpack(raw_val: <P as PackedType>::Type) -> Result<Self, Self::Error> {
//        // implement the unpack using TryFrom<>
//    }
//}

// Ideally the bitfield types would be implemented with a const generic for
// offset and length, but these aren't yet supported.
// See RFC 2000: https://github.com/rust-lang/rust/issues/44580
//
// Instead we use typenum, which does much the same (and possibly more)
//
/// A `BoolBitfield` represents a single bit boolean at any position in a
/// 32-bit word.
///
/// Creation of the bitfield should be done through the [`new`](#method.new)
/// method.
///
/// To create a boolean bitfield `true` in the 24th bit from the right (
/// indexing from 0):
/// ```
/// # use sparrow_bitpacker::BoolBitfield;
/// use typenum::consts::*;
/// let my_bitfield = BoolBitfield::<U24>::new(true);
/// ```
///
/// The final generic M is intended as a marker type to allow easy creation of
/// new-types that are express a complete `BoolBitfield`.
#[derive(PartialEq, Copy, Clone)]
pub struct BoolBitfield<Offset, T = PackedU32, D = B0, M = DefaultNewTypeMarker> {
    val: bool,
    // There's a bit of a debate as to whether PhantomData should be used here
    // in place of just the type. PhantomData affords fewer bounds on the marker
    // types and is almost certainly functionally equivalent.
    _offset_marker: PhantomData<Offset>,
    _t_marker: PhantomData<T>,
    _default_marker: PhantomData<D>,
    _newtype_marker: PhantomData<M>,
}

#[cfg(feature = "std")]
impl<Offset, T, D, M> fmt::Debug for BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned,
    D: Bit,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct(&format!("BoolBitfield<Offset: {}>", Offset::USIZE))
            .field("val", &self.val)
            .finish()
    }
}

#[allow(deprecated)]
impl<Offset, T, D, M> BitfieldLength for BoolBitfield<Offset, T, D, M> {
    type Length = U1;
}

impl<Offset, T, D, M> BitfieldConfig for BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned,
    D: Bit,
{
    type Length = U1;
    type Offset = Offset;

    const LENGTH: usize = 1;
    const OFFSET: usize = Offset::USIZE;
}

impl<Offset, T, D, M> BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq,
    D: Bit,
{
    /// Constructor for a `BoolBitfield`.
    ///
    /// The offset of the boolean is set using type based constants from
    /// the [`typenum`][typenum] crate.
    /// ```
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    ///
    /// type Offset = U10;
    /// let my_bitfield = BoolBitfield::<Offset>::new(true);
    /// ```
    /// The [typenum constant][typenum::consts] should be `Unsigned` and
    /// strictly less than 32. i.e. U10 is valid, U32 and P10 are invalid.
    /// The validity is checked at compile time and invalid offsets will cause
    /// the compile to fail, hopefully with a useful error message as to why
    /// the constant is invalid.
    ///
    /// The following will not compile:
    /// ```compile_fail
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    /// let my_bitfield = BoolBitfield::<U32>::new(true);
    /// ```
    /// ```compile_fail
    /// # use sparrow_bitpacker::BoolBitfield;
    /// use typenum::consts::*;
    /// let my_bitfield = BoolBitfield::<P10>::new(true);
    /// ```
    pub const fn new(val: bool) -> BoolBitfield<Offset, T, D, M> {
        BoolBitfield::<Offset, T, D, M> {
            val,
            _offset_marker: PhantomData,
            _t_marker: PhantomData,
            _default_marker: PhantomData,
            _newtype_marker: PhantomData,
        }
    }

    /// Updates the internal value to the provided `val`.
    pub fn update(&mut self, val: bool) {
        self.val = val
    }
}

impl <Offset, T, D, M> BoolBitfield<Offset, T, D, M>
{
    /// Converts bool bitfield into the inner bool.
    pub fn into_inner(self) -> bool {
        self.val
    }

    /// Returns the value this bitfield represents
    pub fn val(&self) -> bool {
        self.val
    }
}

impl<Offset, T, D, M> Bitfield<T> for BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq,
    D: Bit,
{
    type Error = Infallible;

    fn pack(&self) -> Result<T::Type, Self::Error> {
        if self.val {
            let output = T::Type::one();
            Ok(output << NumCast::from(Offset::USIZE).unwrap())
        } else {
            Ok(T::Type::zero())
        }
    }

    fn unpack(raw_val: T::Type) -> Result<BoolBitfield<Offset, T, D, M>, Self::Error> {
        let one = T::Type::one();
        let mask: T::Type = one << NumCast::from(Offset::USIZE).unwrap();
        Ok(Self::new((raw_val & mask) != T::Type::zero()))
    }
}

impl <Offset, T, D, M> From<bool> for BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq,
    D: Bit,
{
    fn from(value: bool) -> Self {
        Self::new(value)
    }
}

impl<Offset, T, D, M> From<BoolBitfield<Offset, T, D, M>> for bool
{
    fn from(value: BoolBitfield<Offset, T, D, M>) -> Self {
        value.val()
    }
}

impl<Offset, T, D, M> From<&BoolBitfield<Offset, T, D, M>> for bool
{
    fn from(value: &BoolBitfield<Offset, T, D, M>) -> Self {
        value.val()
    }
}

impl<Offset, T, D, M> Default for BoolBitfield<Offset, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq,
    D: Bit,
{
    fn default() -> Self {
        Self::new(D::to_bool())
    }
}

macro_rules! try_from_uint_bool {
    ($source:ty) => {
        impl <Offset, T, D, M> TryFrom<$source> for BoolBitfield<Offset, T, D, M>
            where
                Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
                T: PackedType + PartialEq,
                D: Bit,
        {
            type Error = BitfieldError;

            fn try_from(value: $source) -> Result<Self, Self::Error> {
                match value {
                    0 => Ok(false.into()),
                    1 => Ok(true.into()),
                    _ => Err(BitfieldError::InvalidNonBoolConversionFromUintToBool),
                }
            }
        }
    };
}

try_from_uint_bool!(u8);
try_from_uint_bool!(u16);
try_from_uint_bool!(u32);
try_from_uint_bool!(u64);
try_from_uint_bool!(u128);

/// A `UintBitfield` represents an unsigned integer bitfield of arbitrary length
/// up to [T::PackedLength](PackedType::PackedLength) at any position in a T::PackedLength-bit word,
/// subject to the bitfield fitting within the type [T::Type](PackedType::Type)
/// (which defaults to u32 through the default `T` of [PackedU32]).
///
/// The protection on the bitfield correctness is enforced through compile
/// time type constraints and all creations being done through the
/// [`new`](#method.new) method. Extensive use is made of [typenum]
/// to enforce the compile time trait bounds.
/// ```
/// use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VAL: u32 = 13;
/// type Offset = U22;
/// type Length = U5;
/// let my_bitfield = UintBitfield::<Offset, Length>::new(VAL).unwrap();
/// ```
/// In this case, the default `T` of [PackedU32] is used. So,
/// if the given offset and the length result in a bitfield that will not
/// fit inside a u32, the compile will fail. Specifically, the sum of the
/// offset and the length must be less than or equal to 32.
///
/// So the following will fail:
/// ```compile_fail
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 15;
/// type Offset = U28;
/// type Length = U5; // 28 + 5 = 33 (> 32)
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// But the following is ok:
/// ```
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 15;
/// type Offset = U28;
/// type Length = U4; // 28 + 4 = 32 (ok!)
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// Zero length bitfields are also disallowed and cause a compile error.
/// ```compile_fail
/// # use sparrow_bitpacker::UintBitfield;
/// use typenum::consts::*;
/// const VALUE: u32 = 0;
/// type Offset = U20;
/// type Length = U0;
/// let res = UintBitfield::<Offset, Length>::new(VALUE);
/// ```
/// Changing the packed type is easy, and the main uint types are defined as
/// simple full-width packing types:
/// ```
/// use typenum::consts::U6;
/// use sparrow_bitpacker::{UintBitfield, PackedU16};
///
/// const VALUE: u16 = 15;
/// type Offset = U6;
/// type Length = U6;
/// let u16_bitfield = UintBitfield::<Offset, Length, PackedU16>::new(VALUE).unwrap();
/// ```
/// It is also possible to define a different type for the packing by implementing
/// the [PackedType] trait. For example in the case in which we want to use and
/// restrict access to only 9 bits of a 16-bit uint:
/// ```
/// // TypeLength is the extension traits to provide the uint types
/// // with length information
/// use typenum::consts::*;
/// use sparrow_bitpacker::{UintBitfield, TypeLength, PackedType};
///
/// #[derive(PartialEq, Copy, Clone)]
/// struct PackedU9 {}
///
/// impl PackedType for PackedU9 {
///     type Type = u16;
///     type PackedLength = U9;
/// }
///
/// const VALUE: u16 = 15;
/// type Offset = U3;
/// type Length = U6;
/// let u9_bitfield = UintBitfield::<Offset, Length, PackedU9>::new(VALUE).unwrap();
/// ```
/// Using such a [PackedType] would result in a compile failure if the bitfield
/// does not fit inside [PackedType::PackedLength]:
/// ```compile_fail
/// # use typenum::consts::*;
/// # use sparrow_bitpacker::{UintBitfield, TypeLength, PackedType};
/// #
/// # #[derive(PartialEq, Copy, Clone)]
/// # struct PackedU9 {}
/// # impl PackedType for PackedU9 {
/// #     type Type = u16;
/// #     type PackedLength = U9;
/// # }
/// #
/// const VALUE: u16 = 15;
/// type Offset = U6;
/// type Length = U4; // <- The length + offset > 9 so fails (but note, is less than 16)
/// let u9_bitfield = UintBitfield::<Offset, Length, PackedU9>::new(VALUE).unwrap();
/// ```
#[derive(PartialEq, Copy, Clone)]
pub struct UintBitfield<Offset, Length, T = PackedU32, D = U0, M = DefaultNewTypeMarker>
where
    T: PackedType,
    <T as PackedType>::Type: PartialEq,
{
    val: T::Type,
    _offset_marker: PhantomData<Offset>,
    _length_marker: PhantomData<Length>,
    _t_marker: PhantomData<T>,
    _default_marker: PhantomData<D>,
    _newtype_marker: PhantomData<M>,
}

#[cfg(feature = "std")]
impl<Offset, Length, T, D, M> fmt::Debug for UintBitfield<Offset, Length, T, D, M>
where
    T: PackedType + PartialEq,
    Offset: Unsigned,
    Length: Unsigned,
    D: Unsigned,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let struct_type_str = format!(
            "UintBitfield<Offset: {}, Length: {}>",
            Offset::USIZE,
            Length::USIZE
        );
        f.debug_struct(&struct_type_str)
            .field("val", &self.val)
            .finish()
    }
}

#[allow(deprecated)]
impl<Offset, Length, T, D, M> BitfieldLength for UintBitfield<Offset, Length, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero,
    <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32
    T: PackedType + PartialEq,
    D: Unsigned,
{
    type Length = Length;
}

impl<Offset, Length, T, D, M> BitfieldConfig for UintBitfield<Offset, Length, T, D, M>
where
    Offset: Unsigned,
    Length: Unsigned,
    T: PackedType + PartialEq,
    D: Unsigned,
{
    type Length = Length;
    type Offset = Offset;

    const OFFSET: usize = Offset::USIZE;
    const LENGTH: usize = Length::USIZE;
}

macro_rules! const_uint_bitfield_max {
    ($packed_type:ty) => {
        impl<Offset, Length, D, M> UintBitfield<Offset, Length, $packed_type, D, M>
            where
                Offset: Unsigned + Cmp<<$packed_type as PackedType>::PackedLength, Output = Less>,
                Length: Unsigned + Cmp<Sum<<$packed_type as PackedType>::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero + Add<U1>,
                <Length as Add<Offset>>::Output: Cmp<Sum<<$packed_type as PackedType>::PackedLength, U1>, Output = Less>, // That is <= 32
                D: Unsigned + Len,
                <D as Len>::Output: Cmp<Sum<Length, U1>, Output = Less>,
                {
                    pub const MAX_INT: <$packed_type as PackedType>::Type = <$packed_type as PackedType>::Type::MAX >> (<$packed_type as PackedType>::Type::BITS - Length::U32);

                    pub const MAX: Self = UintBitfield::<Offset, Length, $packed_type, D, M> {
                        val: Self::MAX_INT,
                        _offset_marker: PhantomData,
                        _length_marker: PhantomData,
                        _t_marker: PhantomData,
                        _default_marker: PhantomData,
                        _newtype_marker: PhantomData,
                    };

                    pub const fn const_new(val: <$packed_type as PackedType>::Type)
                        -> BitfieldResult<UintBitfield<Offset, Length, $packed_type, D, M>>
                    {
                        if val > Self::MAX_INT {
                            Err(BitfieldError::OverRange)
                        } else {
                            Ok(Self::new_unchecked(val))
                        }
                    }
                }
    }
}

const_uint_bitfield_max!(PackedU8);
const_uint_bitfield_max!(PackedU16);
const_uint_bitfield_max!(PackedU32);
const_uint_bitfield_max!(PackedU64);
const_uint_bitfield_max!(PackedU128);

impl<Offset, Length, T, D, M> UintBitfield<Offset, Length, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero + Add<U1>,
    <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32
    T: PackedType + PartialEq,
    D: Unsigned + Len,
    <D as Len>::Output: Cmp<Sum<Length, U1>, Output = Less>,
    // The above traits are rather subtle.
    // The Offset must be less than T::PackedLength
    // The Length must be less than T::PackedLength + 1
    // The Length + Offset must be less than T::PackedLength + 1
    // Length requires the Add trait so it can be represented as such. We omit
    // to use the Sum<> notation as Add makes it explicit why we include the Add
    // trait with the Length bounds.
    //
    // The default value must fit inside length, which means it must be less
    // than Length + 1.
{

    /// Constructor for a `UintBitfield`, protecting against invalid
    /// arguments.
    ///
    /// The offset and length of the unsigned integer is set using type based
    /// constants from the [`typenum`][typenum] crate.
    /// ```
    /// # use sparrow_bitpacker::UintBitfield;
    /// use typenum::consts::*;
    /// const VALUE: u32 = 15;
    /// type Offset = U10;
    /// type Length = U6;
    /// let my_bitfield = UintBitfield::<Offset, Length>::new(VALUE).unwrap();
    /// ```
    ///
    /// If the value cannot fit inside the bitfield given the length, a
    /// [`Bitfield::OverRange`](enum.BitfieldError.html#variant.OverRange)
    /// will be raised.
    /// ```
    /// # use sparrow_bitpacker::{UintBitfield, BitfieldError};
    /// use typenum::consts::*;
    /// use typenum::Unsigned;
    /// const VALUE: u32 = 95;
    /// type Offset = U20;
    /// type Length = U6;
    /// let max_val = 2_u32.pow(Length::to_u32()) - 1;
    /// let res = UintBitfield::<Offset, Length>::new(VALUE);
    ///
    /// // We don't need to worry about the exact error message...
    /// if let Err(error) = res {
    ///     if let BitfieldError::OverRange = error {
    ///         assert!(true);
    ///     } else {
    ///         panic!("The error should be a BitfieldError::OverRange");
    ///     }
    /// } else {
    ///     panic!("The bitfield should error");
    /// }
    /// ```
    pub fn new(val: T::Type) -> BitfieldResult<UintBitfield<Offset, Length, T, D, M>> {

        Self::check_val(val)?;

        Ok(Self::new_unchecked(val))
    }

    const fn new_unchecked(val: T::Type) -> UintBitfield<Offset, Length, T, D, M> {

        UintBitfield::<Offset, Length, T, D, M> {
            val,
            _offset_marker: PhantomData,
            _length_marker: PhantomData,
            _t_marker: PhantomData,
            _default_marker: PhantomData,
            _newtype_marker: PhantomData,
        }
    }

    /// Updates the UintBitfield with the provided new value, checking the
    /// value is within range equivalently to `new()`.
    pub fn update(&mut self, val: T::Type) -> BitfieldResult<()>
    {
        Self::check_val(val)?;
        self.val = val;

        Ok(())
    }

    fn check_val(val: T::Type) -> BitfieldResult<()>
    {
        let zero = T::Type::zero();
        let ones = !zero;

        // To get the max value, we need to zero the upper bits between the
        // packed length and the full length using a left shift, then shift
        // the whole thing down using a right shift so that we only have bits
        // corresponding to a full bitfield.
        let shift_left =
            NumCast::from(<T::Type as TypeLength>::Length::USIZE - T::PackedLength::USIZE)
                .expect("This should never fail and so is a bug.");

        let shift_right = shift_left
            + NumCast::from(T::PackedLength::USIZE - Length::USIZE)
                .expect("This should never fail and so is a bug.");

        let max_val = (ones << shift_left) >> shift_right;

        if val > max_val {
            Err(BitfieldError::OverRange)
        } else {
            Ok(())
        }

    }
}

macro_rules! try_from_uint {
    // Creates the TryFrom<> block for the requested PackedType::Type
    ($source:ty, $packed:ty) => {
        impl<Offset, Length, D, M> TryFrom<$source> for UintBitfield<Offset, Length, $packed, D, M>
        where
            Offset: Unsigned + Cmp<<$packed as PackedType>::PackedLength, Output = Less>,
            Length: Unsigned + Cmp<Sum<<$packed as PackedType>::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero + Add<U1>,
            <Length as Add<Offset>>::Output: Cmp<Sum<<$packed as PackedType>::PackedLength, U1>, Output = Less>,
            D: Unsigned + Len,
            <D as Len>::Output: Cmp<Sum<Length, U1>, Output = Less>,
        {
            type Error = BitfieldError;

            fn try_from(value: $source) -> Result<Self, Self::Error> {
                Self::new(value.into())
            }
        }
    }
}

try_from_uint!(u8, PackedU8);
try_from_uint!(u8, PackedU16);
try_from_uint!(u8, PackedU32);
try_from_uint!(u8, PackedU64);
try_from_uint!(u8, PackedU128);

try_from_uint!(u16, PackedU16);
try_from_uint!(u16, PackedU32);
try_from_uint!(u16, PackedU64);
try_from_uint!(u16, PackedU128);

try_from_uint!(u32, PackedU32);
try_from_uint!(u32, PackedU64);
try_from_uint!(u32, PackedU128);

try_from_uint!(u64, PackedU64);
try_from_uint!(u64, PackedU128);

try_from_uint!(u128, PackedU128);

impl <Offset, Length, T, D, M> UintBitfield<Offset, Length, T, D, M>
where
    T: PackedType,
{
    /// Converts uint bitfield into a uint of [`T::Type`].
    pub fn into_inner(self) -> T::Type {
        self.val
    }
}

impl <Offset, Length, T, D, M> UintBitfield<Offset, Length, T, D, M>
where
    T: PackedType + Copy, // Copy is required by PackedType, but we add defensively
{
    /// Returns the value this bitfield represents
    pub fn val(&self) -> T::Type {
        self.val
    }
}

macro_rules! from_uintbitfield {
    // Creates the From<> block for a UintBitfield for the requested output type
    ($target_type:ty) => {
        impl<Offset, Length, T, D, M> From<UintBitfield<Offset, Length, T, D, M>> for $target_type
            where
                T: PackedType,
                <T as PackedType>::Type: AsPrimitive<$target_type>,
                <$target_type as TypeLength>::Length: IsGreaterOrEqual<Length, Output = True>
            {
                fn from(bitfield: UintBitfield<Offset, Length, T, D, M>) -> Self {
                    bitfield.into_inner().as_()
                }
            }

        impl<Offset, Length, T, D, M> From<&UintBitfield<Offset, Length, T, D, M>> for $target_type
            where
                T: PackedType + Copy,
                <T as PackedType>::Type: AsPrimitive<$target_type>,
                <$target_type as TypeLength>::Length: IsGreaterOrEqual<Length, Output = True>
            {
                fn from(bitfield: &UintBitfield<Offset, Length, T, D, M>) -> Self {
                    bitfield.val().as_()
                }
            }
    }
}

from_uintbitfield!(u128);
from_uintbitfield!(u64);
from_uintbitfield!(u32);
from_uintbitfield!(u16);
from_uintbitfield!(u8);

// FIXME possibly switch to using IsLess, IsLessOrEqual etc in typenum::type_operators
// e.g.
//    Offset: Unsigned + IsLess<T::PackedLength, Output = True>,
//    Length: Unsigned + IsLessOrEqual<T::PackedLength, Output = True> + Add<Offset> + NonZero,
//    <Length as Add<Offset>>::Output: IsLessOrEqual<T::PackedLength, Output = True>,
//    T: PackedType + PartialEq,
//    D: Unsigned + Len,
//    <D as Len>::Output: IsLessOrEqual<Length, Output = True>,
//
//    Note that the bounds above result in a slightly less ergonomic output, so
//    might not be desirable. Also, will require fixing all the failure tests.
impl<Offset, Length, T, D, M> Bitfield<T> for UintBitfield<Offset, Length, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero + Add<U1>,
    <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>, // That is <= 32
    T: PackedType + PartialEq,
    D: Unsigned + Len,
    <D as Len>::Output: Cmp<Sum<Length, U1>, Output = Less>, // The default must fit inside Length
{
    type Error = Infallible;

    fn pack(&self) -> Result<T::Type, Self::Error> {
        Ok(self.val << NumCast::from(Offset::USIZE).unwrap())
    }

    fn unpack(raw_val: T::Type) -> Result<UintBitfield<Offset, Length, T, D, M>, Self::Error> {
        let zero = T::Type::zero();
        let mask: T::Type = (!zero
            >> NumCast::from(<T::Type as TypeLength>::Length::USIZE - Length::USIZE).unwrap())
            << NumCast::from(Offset::USIZE).unwrap();

        let shifted_val = mask & raw_val;
        Ok(Self::new_unchecked(shifted_val >> NumCast::from(Offset::USIZE).unwrap()))
    }
}

impl<Offset, Length, T, D, M> Default for UintBitfield<Offset, Length, T, D, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    Length: Unsigned + Cmp<Sum<T::PackedLength, U1>, Output = Less> + Add<Offset> + NonZero + Add<U1>,
    <Length as Add<Offset>>::Output: Cmp<Sum<T::PackedLength, U1>, Output = Less>,
    T: PackedType + PartialEq,
    D: Unsigned + Len + ToInt<T::Type>,
    <D as Len>::Output: Cmp<Sum<Length, U1>, Output = Less>, // The default must fit inside Length
{
    fn default() -> Self {
        Self::new(<D as ToInt<T::Type>>::to_int()).expect("Error getting the default. This is a bug.")
    }
}


#[derive(Debug, PartialEq, Copy, Clone)]
pub struct ConstHiBitfield<Offset, T = PackedU32, M = DefaultNewTypeMarker>
where Offset: Unsigned,
{
    inner: BoolBitfield<Offset, T, B1, M>
}

impl <Offset, T, M> ConstHiBitfield<Offset, T, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq
{
    pub const fn new() -> Self {
        ConstHiBitfield {
            inner: BoolBitfield::<Offset, T, B1, M>::new(true)
        }
    }

    /// Returns the inner value contained by this bitfield.
    pub fn into_inner(self) -> bool {
        self.inner.into()
    }
}

impl<Offset, T, M> BitfieldConfig for ConstHiBitfield<Offset, T, M>
where
    Offset: Unsigned,
{
    type Length = U1;
    type Offset = Offset;

    const LENGTH: usize = 1;
    const OFFSET: usize = Offset::USIZE;
}


impl<Offset, T, M> Bitfield<T> for ConstHiBitfield<Offset, T, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq,
{
    type Error = BitfieldError;

    fn pack(&self) -> BitfieldResult<T::Type> {
        Ok(Bitfield::pack(&self.inner)?)
    }

    fn unpack(raw_val: <T as PackedType>::Type) -> BitfieldResult<Self> {
        let unpacked_val =
            <BoolBitfield<Offset, T, typenum::B1, M> as Bitfield<T>>::unpack(raw_val)?;

        if !unpacked_val.val {
            return Err(BitfieldError::InvalidConstHiUnpackedValue)
        }
        Ok(Self::new())
    }
}

impl<Offset, T, M> From<ConstHiBitfield<Offset, T, M>> for bool
where
    Offset: Unsigned
{
    fn from(bitfield: ConstHiBitfield<Offset, T, M>) -> Self {
        bitfield.inner.into()
    }
}

impl<Offset, T, M> From<&ConstHiBitfield<Offset, T, M>> for bool
where
    Offset: Unsigned
{
    fn from(bitfield: &ConstHiBitfield<Offset, T, M>) -> Self {
        (&bitfield.inner).into()
    }
}

impl <Offset, T, M> Default for ConstHiBitfield<Offset, T, M>
where
    Offset: Unsigned + Cmp<T::PackedLength, Output = Less>,
    T: PackedType + PartialEq
{
    fn default() -> Self {
        Self::new()
    }
}


#[cfg(test)]
mod bool_bitfield_tests {
    //! The boolean bitfield should contain everything needed to pack
    //! a boolean value into an integer with the default 32-bit output.
    use super::{Bitfield, BoolBitfield, PackedU32, BitfieldConfig};
    #[allow(deprecated)]
    use super::BitfieldLength;

    use typenum::{Unsigned, consts::*};

    #[test]
    /// The BoolBitfield should pack a boolean into the correct location in
    /// a u32.
    fn test_pack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 0);

        let bf = BoolBitfield::<U1>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 1);

        let bf = BoolBitfield::<U10>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 10);

        let bf = BoolBitfield::<U15>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 15);

        let bf = BoolBitfield::<U30>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 30);

        let bf = BoolBitfield::<U31>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 31);

        let bf = BoolBitfield::<U7>::new(false);
        assert_eq!(bf.pack().unwrap(), 0);

        let bf = BoolBitfield::<U21>::new(false);
        assert_eq!(bf.pack().unwrap(), 0);
    }

    #[test]
    /// If update is called on a BoolBitfield, its value should be updated
    /// appropriately.
    fn test_update() {
        let mut bf = BoolBitfield::<U10>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 10);
        assert_eq!(bf.val, true);

        bf.update(false);
        assert_eq!(bf.pack().unwrap(), 0 << 10);
        assert_eq!(bf.val, false);

        bf.update(true);
        assert_eq!(bf.pack().unwrap(), 1 << 10);
        assert_eq!(bf.val, true);
    }

    /// The BoolBitfield should implement the Default trait, which should be
    /// false if the D generic is not set, or should be that which is set
    /// by the D generic (B0: false, B1: true).
    #[test]
    fn test_default() {
        let bf = BoolBitfield::<U10, PackedU32>::default();
        assert_eq!(bf.val, false);

        let bf = BoolBitfield::<U10, PackedU32, B0>::default();
        assert_eq!(bf.val, false);

        let bf = BoolBitfield::<U10, PackedU32, B1>::default();
        assert_eq!(bf.val, true);
    }

    #[test]
    /// The BoolBitfield should implement the Into<bool> trait, supporting
    /// exporting its internal value.
    fn test_into() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0>::new(true);
        // Until type ascription is stable, we need an intermediate variable
        // (type ascription would be `bf.into(): u32`)`
        let into_val: bool = bf.into();
        assert_eq!(into_val, true);
        let bf = BoolBitfield::<U10>::new(true);
        let into_val: bool = bf.into();
        assert_eq!(into_val, true);

        let bf = BoolBitfield::<U7>::new(false);
        let into_val: bool = bf.into();
        assert_eq!(into_val, false);

        let bf = BoolBitfield::<U21>::new(false);
        let into_val: bool = bf.into();
        assert_eq!(into_val, false);
    }

    #[test]
    #[allow(deprecated)]
    /// The BoolBitfield should impement the BitfieldLength trait
    fn test_bitfield_length() {
        // Length is always 1.
        assert_eq!(<BoolBitfield<U0> as BitfieldLength>::Length::USIZE, 1usize);
        assert_eq!(<BoolBitfield<U6> as BitfieldLength>::Length::USIZE, 1usize);
        assert_eq!(<BoolBitfield<U10> as BitfieldLength>::Length::USIZE, 1usize);

        assert_eq!(<BoolBitfield<U20> as BitfieldLength>::Length::U32, 1u32);
    }

    #[test]
    /// The BoolBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct constants as specified by the trait.
    fn test_bitfield_config_consts() {
        // Length is always 1.
        assert_eq!(BoolBitfield::<U0>::LENGTH, 1usize);
        assert_eq!(BoolBitfield::<U10>::LENGTH, 1usize);
        assert_eq!(BoolBitfield::<U6>::LENGTH, 1usize);
        assert_eq!(BoolBitfield::<U20>::LENGTH, 1usize);

        assert_eq!(BoolBitfield::<U0>::OFFSET, 0usize);
        assert_eq!(BoolBitfield::<U10>::OFFSET, 10usize);
        assert_eq!(BoolBitfield::<U6>::OFFSET, 6usize);
        assert_eq!(BoolBitfield::<U20>::OFFSET, 20usize);
    }

    #[test]
    /// The BoolBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct associated types as specified by the trait.
    fn test_bitfield_config_associated_types() {
        // Length is always 1.
        assert_eq!(
            <BoolBitfield::<U0> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <BoolBitfield::<U10> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <BoolBitfield::<U6> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <BoolBitfield::<U20> as BitfieldConfig>::Length::USIZE,
            1usize
        );

        assert_eq!(
            <BoolBitfield::<U0> as BitfieldConfig>::Offset::USIZE,
            0usize
        );
        assert_eq!(
            <BoolBitfield::<U10> as BitfieldConfig>::Offset::USIZE,
            10usize
        );
        assert_eq!(
            <BoolBitfield::<U6> as BitfieldConfig>::Offset::USIZE,
            6usize
        );
        assert_eq!(
            <BoolBitfield::<U20> as BitfieldConfig>::Offset::USIZE,
            20usize
        );
    }

    #[test]
    /// The BoolBitfield should properly implement the BitfieldConfig trait
    /// which provides a method to lookup the associated constant values
    /// on instances of the bitfield.
    fn test_bitfield_config_methods() {
        // Length is always 1.
        let bitfield = BoolBitfield::<U0>::new(false);
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 0usize);

        let bitfield = BoolBitfield::<U10>::new(false);
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 10usize);

        let bitfield = BoolBitfield::<U6>::new(false);
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 6usize);

        let bitfield = BoolBitfield::<U20>::new(false);
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 20usize);
    }

    #[test]
    /// The BoolBitfield should implement into_inner, supporting
    /// exporting its internal value.
    fn test_into_inner() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0>::new(true);
        // Until type ascription is stable, we need an intermediate variable
        // (type ascription would be `bf.into_inner(): u32`)`
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

        let bf = BoolBitfield::<U10>::new(true);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

        let bf = BoolBitfield::<U7>::new(false);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, false);

        let bf = BoolBitfield::<U21>::new(false);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, false);
    }

    #[test]
    /// The BoolBitfield should unpack bitfields to the correct bitfield object
    fn test_unpack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0>::new(true);
        assert_eq!(BoolBitfield::<U0>::unpack(1u32 << 0).unwrap(), bf);

        let bf = BoolBitfield::<U1>::new(true);
        assert_eq!(BoolBitfield::<U1>::unpack(1u32 << 1).unwrap(), bf);

        let bf = BoolBitfield::<U7>::new(true);
        assert_eq!(BoolBitfield::<U7>::unpack(1u32 << 7).unwrap(), bf);

        let bf = BoolBitfield::<U25>::new(true);
        assert_eq!(BoolBitfield::<U25>::unpack(1u32 << 25).unwrap(), bf);

        let bf = BoolBitfield::<U31>::new(true);
        assert_eq!(BoolBitfield::<U31>::unpack(1u32 << 31).unwrap(), bf);

        // A couple of false tests
        let bf = BoolBitfield::<U3>::new(false);
        assert_eq!(BoolBitfield::<U3>::unpack(0).unwrap(), bf);

        let bf = BoolBitfield::<U29>::new(false);
        assert_eq!(BoolBitfield::<U29>::unpack(0).unwrap(), bf);
    }

    #[test]
    /// Unpacking a word should ignore bits outside of the bitfield
    fn test_unpack_with_extra_bits() {
        // Unpacks to true
        let val = (1u32 << 31) | (1u32 << 19) | (1u32 << 30);
        let bf = BoolBitfield::<U31>::new(true);
        assert_eq!(BoolBitfield::<U31>::unpack(val).unwrap(), bf);

        // And false
        let val = (1u32 << 31) | (1u32 << 19) | (1u32 << 30);
        let bf = BoolBitfield::<U12>::new(false);
        assert_eq!(BoolBitfield::<U12>::unpack(val).unwrap(), bf);
    }

}

#[cfg(test)]
mod bool_bitfield_u16_tests {
    //! The boolean bitfield should contain everything needed to pack
    //! a boolean value into an explicitly set PackedU16 packed type.
    use crate::PackedU16;
    use super::{Bitfield, BoolBitfield};

    use typenum::consts::*;

    #[test]
    /// The BoolBitfield should pack a boolean into the correct location in
    /// a u16.
    fn test_pack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0, PackedU16>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 0);

        let bf = BoolBitfield::<U1, PackedU16>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 1);

        let bf = BoolBitfield::<U10, PackedU16>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 10);

        let bf = BoolBitfield::<U15, PackedU16>::new(true);
        assert_eq!(bf.pack().unwrap(), 1 << 15);

        let bf = BoolBitfield::<U7, PackedU16>::new(false);
        assert_eq!(bf.pack().unwrap(), 0);

        let bf = BoolBitfield::<U13, PackedU16>::new(false);
        assert_eq!(bf.pack().unwrap(), 0);
    }

    #[test]
    /// The BoolBitfield should implement into_inner, supporting
    /// exporting its internal value.
    fn test_into_inner() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0, PackedU16>::new(true);
        // Until type ascription is stable, we need an intermediate variable
        // (type ascription would be `bf.into_inner(): u32`)`
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

        let bf = BoolBitfield::<U10, PackedU16>::new(true);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

        let bf = BoolBitfield::<U7, PackedU16>::new(false);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, false);

        let bf = BoolBitfield::<U15, PackedU16>::new(false);
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, false);
    }

    #[test]
    /// The BoolBitfield should unpack bitfields to the correct bitfield object
    fn test_unpack() {
        // We test a few cases, including the limits
        let bf = BoolBitfield::<U0, PackedU16>::new(true);
        assert_eq!(
            BoolBitfield::<U0, PackedU16>::unpack(1u16 << 0).unwrap(),
            bf
        );

        let bf = BoolBitfield::<U1, PackedU16>::new(true);
        assert_eq!(
            BoolBitfield::<U1, PackedU16>::unpack(1u16 << 1).unwrap(),
            bf
        );

        let bf = BoolBitfield::<U7, PackedU16>::new(true);
        assert_eq!(
            BoolBitfield::<U7, PackedU16>::unpack(1u16 << 7).unwrap(),
            bf
        );

        let bf = BoolBitfield::<U15, PackedU16>::new(true);
        assert_eq!(
            BoolBitfield::<U15, PackedU16>::unpack(1u16 << 15).unwrap(),
            bf
        );

        // A couple of false tests
        let bf = BoolBitfield::<U3, PackedU16>::new(false);
        assert_eq!(BoolBitfield::<U3, PackedU16>::unpack(0).unwrap(), bf);

        let bf = BoolBitfield::<U12, PackedU16>::new(false);
        assert_eq!(BoolBitfield::<U12, PackedU16>::unpack(0).unwrap(), bf);
    }

    #[test]
    /// Unpacking a word should ignore bits outside of the bitfield
    fn test_unpack_with_extra_bits() {
        // Unpacks to true
        let val = (1u16 << 15) | (1u16 << 12) | (1u16 << 8);
        let bf = BoolBitfield::<U15, PackedU16>::new(true);
        assert_eq!(BoolBitfield::<U15, PackedU16>::unpack(val).unwrap(), bf);

        // And false
        let val = (1u16 << 15) | (1u16 << 12) | (1u16 << 8);
        let bf = BoolBitfield::<U4, PackedU16>::new(false);
        assert_eq!(BoolBitfield::<U4, PackedU16>::unpack(val).unwrap(), bf);
    }
}

#[cfg(test)]
mod uint_bitfield_tests {
    //! The UintBitfield should contain everything needed to pack
    //! a uint value into an integer.

    use crate::{PackedU128, PackedU16, PackedType};

    use super::{Bitfield, UintBitfield, PackedU32, BitfieldConfig, BitfieldError};
    #[allow(deprecated)]
    use super::BitfieldLength;

    use typenum::{Unsigned, consts::*};
    use rand::{thread_rng, Rng};

    #[test]
    /// The UintBitfield should pack a value into the correct location in
    /// a u32.
    ///
    /// Invalid update values should result in a BitfieldError::OverRange
    fn test_pack() {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        // Start with a few corner cases
        // Full width, all ones
        let val: u32 = ALL_ONES;
        let bf = UintBitfield::<U0, U32>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);

        // Hard against upper bound
        let val: u32 = ALL_ONES >> 4;
        let bf = UintBitfield::<U4, U28>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 4);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);

        // Check a few random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 10);

        let val: u32 = rng.gen_range(0, 2u32.pow(15));
        let bf = UintBitfield::<U17, U15>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 17);

        let val: u32 = rng.gen_range(0, 2u32.pow(10));
        let bf = UintBitfield::<U0, U10>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val);

    }


    #[test]
    /// If update is called on a BoolBitfield, its value should be updated
    /// appropriately.
    ///
    /// Invalid update values should result in a BitfieldError::OverRange
    fn test_update() {

        let val = 1021;
        let mut bf = UintBitfield::<U17, U15>::new(val).unwrap();
        assert_eq!(bf.pack().unwrap(), val << 17);
        assert_eq!(bf.val, val);

        let new_val = 993;
        bf.update(new_val).unwrap();
        assert_eq!(bf.pack().unwrap(), new_val << 17);
        assert_eq!(bf.val, new_val);

        assert!(matches!(bf.update(2u32.pow(22)), Err(BitfieldError::OverRange)));

    }

    /// The UintBitfield should implement the Default trait, which should be
    /// 0.
    #[test]
    fn test_default() {
        // Firstly do the default default
        let bf = UintBitfield::<U0, U10>::default();
        assert_eq!(bf.val, 0u32);

        let u15_default = UintBitfield::<U0, U10, PackedU32, U15>::default();
        assert_eq!(u15_default.val, 15u32);
    }

    #[allow(deprecated)]
    #[test]
    /// The UintBitfield should impement the BitfieldLength trait
    fn test_bitfield_length() {
        // Length is set with the Length generic parameter
        assert_eq!(
            <UintBitfield<U0, U10> as BitfieldLength>::Length::USIZE,
            10usize
        );
        assert_eq!(
            <UintBitfield<U5, U5> as BitfieldLength>::Length::USIZE,
            5usize
        );
        assert_eq!(<UintBitfield<U12, U8> as BitfieldLength>::Length::U16, 8u16);
    }

    #[test]
    /// The UintBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct constants as specified by the trait.
    fn test_bitfield_config_consts() {
        assert_eq!(UintBitfield::<U0, U10>::OFFSET, 0usize);
        assert_eq!(UintBitfield::<U0, U10>::LENGTH, 10usize);

        assert_eq!(UintBitfield::<U10, U10>::OFFSET, 10usize);
        assert_eq!(UintBitfield::<U10, U10>::LENGTH, 10usize);

        assert_eq!(UintBitfield::<U6, U15>::OFFSET, 6usize);
        assert_eq!(UintBitfield::<U6, U15>::LENGTH, 15usize);

        assert_eq!(UintBitfield::<U15, U6>::OFFSET, 15usize);
        assert_eq!(UintBitfield::<U15, U6>::LENGTH, 6usize);
    }

    #[test]
    /// The UintBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct associated types as specified by the trait.
    fn test_bitfield_config_associated_types() {
        assert_eq!(
            <UintBitfield::<U0, U10> as BitfieldConfig>::Offset::USIZE,
            0usize
        );
        assert_eq!(
            <UintBitfield::<U0, U10> as BitfieldConfig>::Length::USIZE,
            10usize
        );

        assert_eq!(
            <UintBitfield::<U10, U10> as BitfieldConfig>::Offset::USIZE,
            10usize
        );
        assert_eq!(
            <UintBitfield::<U10, U10> as BitfieldConfig>::Length::USIZE,
            10usize
        );

        assert_eq!(
            <UintBitfield::<U6, U15> as BitfieldConfig>::Offset::USIZE,
            6usize
        );
        assert_eq!(
            <UintBitfield::<U6, U15> as BitfieldConfig>::Length::USIZE,
            15usize
        );

        assert_eq!(
            <UintBitfield::<U15, U6> as BitfieldConfig>::Offset::USIZE,
            15usize
        );
        assert_eq!(
            <UintBitfield::<U15, U6> as BitfieldConfig>::Length::USIZE,
            6usize
        );
    }

    #[test]
    /// The UintBitfield should properly implement the BitfieldConfig trait
    /// which provides a method to lookup the associated constant values
    /// on instances of the bitfield.
    fn test_bitfield_config_methods() {
        let bitfield = UintBitfield::<U0, U10>::new(5u32).unwrap();
        assert_eq!(bitfield.length(), 10usize);
        assert_eq!(bitfield.offset(), 0usize);

        let bitfield = UintBitfield::<U10, U10>::new(12u32).unwrap();
        assert_eq!(bitfield.length(), 10usize);
        assert_eq!(bitfield.offset(), 10usize);

        let bitfield = UintBitfield::<U6, U13>::new(99u32).unwrap();
        assert_eq!(bitfield.length(), 13usize);
        assert_eq!(bitfield.offset(), 6usize);

        let bitfield = UintBitfield::<U16, U3>::new(3u32).unwrap();
        assert_eq!(bitfield.length(), 3usize);
        assert_eq!(bitfield.offset(), 16usize);
    }

    #[test]
    /// The UintBitfield should provide access to the inner type, either through
    /// the `val` method, through `into_inner` or through From<>,
    fn test_conversion() {
        let mut rng = thread_rng();

        // Check a few random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(bf.val(), val);
        assert_eq!((&bf).val(), val);
        assert_eq!(Into::<u32>::into(&bf), val);
        assert_eq!(Into::<u32>::into(bf.clone()), val);
        assert_eq!(Into::<u16>::into(&bf), val as u16);
        assert_eq!(Into::<u16>::into(bf.clone()), val as u16);
        assert_eq!(Into::<u8>::into(&bf), val as u8);
        assert_eq!(Into::<u8>::into(bf.clone()), val as u8);
        assert_eq!(bf.into_inner(), val);

        let val: u32 = rng.gen_range(0, 2u32.pow(15));
        let bf = UintBitfield::<U17, U15>::new(val).unwrap();
        assert_eq!(bf.val(), val);
        assert_eq!((&bf).val(), val);
        assert_eq!(Into::<u32>::into(&bf), val);
        assert_eq!(Into::<u32>::into(bf.clone()), val);
        assert_eq!(Into::<u64>::into(&bf), val as u64);
        assert_eq!(Into::<u64>::into(bf.clone()), val as u64);
        assert_eq!(bf.into_inner(), val);

        let val: u32 = rng.gen_range(0, 2u32.pow(10));
        let bf = UintBitfield::<U0, U10>::new(val).unwrap();
        assert_eq!(bf.val(), val);
        assert_eq!((&bf).val(), val);
        assert_eq!(Into::<u32>::into(&bf), val);
        assert_eq!(Into::<u32>::into(bf.clone()), val);
        assert_eq!(Into::<u16>::into(&bf), val as u16);
        assert_eq!(Into::<u16>::into(bf.clone()), val as u16);
        assert_eq!(bf.into_inner(), val);

        let val: u16 = rng.gen_range(0, 2u16.pow(10));
        let bf = UintBitfield::<U0, U10, PackedU16>::new(val).unwrap();
        assert_eq!(bf.val(), val);
        assert_eq!((&bf).val(), val);
        assert_eq!(Into::<u32>::into(&bf), val as u32);
        assert_eq!(Into::<u32>::into(bf.clone()), val as u32);
        assert_eq!(Into::<u16>::into(&bf), val);
        assert_eq!(Into::<u16>::into(bf.clone()), val);
        assert_eq!(bf.into_inner(), val);

        let val: u128 = rng.gen_range(0, 2u128.pow(10));
        let bf = UintBitfield::<U100, U10, PackedU128>::new(val).unwrap();
        assert_eq!(bf.val(), val);
        assert_eq!((&bf).val(), val);
        assert_eq!(Into::<u128>::into(&bf), val);
        assert_eq!(Into::<u128>::into(bf.clone()), val);
        assert_eq!(Into::<u32>::into(&bf), val as u32);
        assert_eq!(Into::<u32>::into(bf.clone()), val as u32);
        assert_eq!(bf.into_inner(), val);
    }

    #[test]
    /// Invalid values should result in a BitfieldError::OverRange
    fn test_invalid_val() {
        const ALL_ONES: u32 = !0;

        let val: u32 = ALL_ONES;
        let res = UintBitfield::<U0, U31>::new(val);

        assert!(matches!(res, Err(BitfieldError::OverRange)));

        let val: u32 = 0b11;
        let res = UintBitfield::<U0, U1>::new(val);
        assert!(matches!(res, Err(BitfieldError::OverRange)));

        let val: u32 = ALL_ONES >> 15;
        let res = UintBitfield::<U10, U12>::new(val);
        assert!(matches!(res, Err(BitfieldError::OverRange)));

        let _ = UintBitfield::<U0, U10>::new(1023).unwrap(); // Ok
        let res = UintBitfield::<U0, U10>::new(1024);
        assert!(matches!(res, Err(BitfieldError::OverRange)));
    }

    #[test]
    /// The UintBitfield should unpack a word to the correct bitfield object.
    fn test_unpack() {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        // Start with a few corner cases
        // Full width, all ones
        let val: u32 = ALL_ONES;
        let bf = UintBitfield::<U0, U32>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U32>::unpack(val).unwrap(), bf);

        // Hard against upper bound
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U5, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U5, U27>::unpack(val << 5).unwrap(), bf);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U27>::unpack(val).unwrap(), bf);

        // Check a couple of random values
        let val: u32 = rng.gen_range(0, 2u32.pow(6));
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(UintBitfield::<U10, U6>::unpack(val << 10).unwrap(), bf);

        let val: u32 = rng.gen_range(0, 2u32.pow(12));
        let bf = UintBitfield::<U15, U12>::new(val).unwrap();
        assert_eq!(UintBitfield::<U15, U12>::unpack(val << 15).unwrap(), bf);
    }

    #[test]
    /// The UintBitfield should ignore bits that are not in the bitfield
    /// when unpacking
    fn test_unpack_with_extra_bits() {
        let mut rng = thread_rng();
        const ALL_ONES: u32 = !0;

        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U5, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U5, U27>::unpack(ALL_ONES).unwrap(), bf);

        // No offset
        let val: u32 = ALL_ONES >> 5;
        let bf = UintBitfield::<U0, U27>::new(val).unwrap();
        assert_eq!(UintBitfield::<U0, U27>::unpack(ALL_ONES).unwrap(), bf);

        // Check a couple of random values
        let full_range_rand: u32 = rng.gen();
        let mask = ALL_ONES >> (32 - 6);
        let val: u32 = (full_range_rand >> 10) & mask;
        let bf = UintBitfield::<U10, U6>::new(val).unwrap();
        assert_eq!(UintBitfield::<U10, U6>::unpack(val << 10).unwrap(), bf);

        let full_range_rand: u32 = rng.gen();
        let mask = ALL_ONES >> (32 - 12);
        let val: u32 = (full_range_rand >> 15) & mask;
        let bf = UintBitfield::<U15, U12>::new(val).unwrap();
        assert_eq!(UintBitfield::<U15, U12>::unpack(val << 15).unwrap(), bf);
    }

    #[test]
    ///
    fn test_custom_packed_type() {
        let packed_data = 0xFFFFFFFF;

        #[derive(Debug, PartialEq)]
        struct Packed24;
        impl PackedType for Packed24 {
            type Type = u32;
            type PackedLength = U24;
        }

        let bf1 = UintBitfield::<U8, U8, Packed24>::unpack(packed_data).unwrap();
        assert_eq!(bf1.val, 0xFF);

        let bf2 = UintBitfield::<U8, U8, Packed24>::new(0xFF).unwrap();
        assert_eq!(bf2.pack().unwrap(), 0xFF << 8);

        assert_eq!(bf1, bf2);

        // The default should also work
        let bf = UintBitfield::<U8, U8, Packed24, U37>::default();
        assert_eq!(bf.val, 37u32);
    }

}

#[cfg(test)]
mod uint_bitfield_const {
    //! For UintBitfields that take a PackedU8, PackedU16, PackedU32, PackedU64
    //! or PackedU128 as their packed types, the `MAX` and `MAX_INT` constants
    //! should be defined, as well as an additional `const_new` function that
    //! can be called in a const context.

    use crate::{PackedU8, PackedU16, PackedU32, PackedU64, PackedU128, UintBitfield};
    use typenum::consts::*;

    #[test]
    /// For a PackedU8, the `MAX` constant should return the bitfield set to the
    /// maximum value and the `MAX_INT` constant should return the equivalent
    /// integer value.
    fn test_max_u8() {
        assert_eq!(UintBitfield::<U0, U1, PackedU8>::MAX_INT, 1u8);
        assert_eq!(UintBitfield::<U2, U5, PackedU8>::MAX_INT, 31u8);
        assert_eq!(UintBitfield::<U2, U6, PackedU8>::MAX_INT, 63u8);
        assert_eq!(UintBitfield::<U0, U8, PackedU8>::MAX_INT, u8::MAX);

        assert_eq!(
            UintBitfield::<U0, U1, PackedU8>::new(1u8).unwrap(),
            UintBitfield::<U0, U1, PackedU8>::MAX
        );
        assert_eq!(
            UintBitfield::<U3, U4, PackedU8>::new(15u8).unwrap(),
            UintBitfield::<U3, U4, PackedU8>::MAX
        );
        assert_eq!(
            UintBitfield::<U2, U6, PackedU8>::new(63u8).unwrap(),
            UintBitfield::<U2, U6, PackedU8>::MAX
        );
        assert_eq!(
            UintBitfield::<U0, U8, PackedU8>::new(u8::MAX).unwrap(),
            UintBitfield::<U0, U8, PackedU8>::MAX
        );
    }

    #[test]
    /// For a PackedU16, the `MAX` constant should return the bitfield set to the
    /// maximum value and the `MAX_INT` constant should return the equivalent
    /// integer value.
    fn test_max_u16() {
        assert_eq!(UintBitfield::<U0, U1, PackedU16>::MAX_INT, 1u16);
        assert_eq!(UintBitfield::<U2, U5, PackedU16>::MAX_INT, 31u16);
        assert_eq!(UintBitfield::<U2, U6, PackedU16>::MAX_INT, 63u16);
        assert_eq!(UintBitfield::<U0, U16, PackedU16>::MAX_INT, u16::MAX);

        assert_eq!(
            UintBitfield::<U0, U1, PackedU16>::new(1u16).unwrap(),
            UintBitfield::<U0, U1, PackedU16>::MAX
        );
        assert_eq!(
            UintBitfield::<U3, U4, PackedU16>::new(15u16).unwrap(),
            UintBitfield::<U3, U4, PackedU16>::MAX
        );
        assert_eq!(
            UintBitfield::<U2, U6, PackedU16>::new(63u16).unwrap(),
            UintBitfield::<U2, U6, PackedU16>::MAX
        );
        assert_eq!(
            UintBitfield::<U0, U16, PackedU16>::new(u16::MAX).unwrap(),
            UintBitfield::<U0, U16, PackedU16>::MAX
        );
    }

    #[test]
    /// For a PackedU32, the `MAX` constant should return the bitfield set to the
    /// maximum value and the `MAX_INT` constant should return the equivalent
    /// integer value.
    fn test_max_u32() {
        assert_eq!(UintBitfield::<U0, U1, PackedU32>::MAX_INT, 1u32);
        assert_eq!(UintBitfield::<U2, U5, PackedU32>::MAX_INT, 31u32);
        assert_eq!(UintBitfield::<U2, U6, PackedU32>::MAX_INT, 63u32);
        assert_eq!(UintBitfield::<U0, U32, PackedU32>::MAX_INT, u32::MAX);

        assert_eq!(
            UintBitfield::<U0, U1, PackedU32>::new(1u32).unwrap(),
            UintBitfield::<U0, U1, PackedU32>::MAX
        );
        assert_eq!(
            UintBitfield::<U3, U4, PackedU32>::new(15u32).unwrap(),
            UintBitfield::<U3, U4, PackedU32>::MAX
        );
        assert_eq!(
            UintBitfield::<U2, U6, PackedU32>::new(63u32).unwrap(),
            UintBitfield::<U2, U6, PackedU32>::MAX
        );
        assert_eq!(
            UintBitfield::<U0, U32, PackedU32>::new(u32::MAX).unwrap(),
            UintBitfield::<U0, U32, PackedU32>::MAX
        );
    }

    #[test]
    /// For a PackedU64, the `MAX` constant should return the bitfield set to the
    /// maximum value and the `MAX_INT` constant should return the equivalent
    /// integer value.
    fn test_max_u64() {
        assert_eq!(UintBitfield::<U0, U1, PackedU64>::MAX_INT, 1u64);
        assert_eq!(UintBitfield::<U2, U5, PackedU64>::MAX_INT, 31u64);
        assert_eq!(UintBitfield::<U2, U6, PackedU64>::MAX_INT, 63u64);
        assert_eq!(UintBitfield::<U0, U64, PackedU64>::MAX_INT, u64::MAX);

        assert_eq!(
            UintBitfield::<U0, U1, PackedU64>::new(1u64).unwrap(),
            UintBitfield::<U0, U1, PackedU64>::MAX
        );
        assert_eq!(
            UintBitfield::<U3, U4, PackedU64>::new(15u64).unwrap(),
            UintBitfield::<U3, U4, PackedU64>::MAX
        );
        assert_eq!(
            UintBitfield::<U2, U6, PackedU64>::new(63u64).unwrap(),
            UintBitfield::<U2, U6, PackedU64>::MAX
        );
        assert_eq!(
            UintBitfield::<U0, U64, PackedU64>::new(u64::MAX).unwrap(),
            UintBitfield::<U0, U64, PackedU64>::MAX
        );
    }

    #[test]
    /// For a PackedU128, the `MAX` constant should return the bitfield set to the
    /// maximum value and the `MAX_INT` constant should return the equivalent
    /// integer value.
    fn test_max_u128() {
        assert_eq!(UintBitfield::<U0, U1, PackedU128>::MAX_INT, 1u128);
        assert_eq!(UintBitfield::<U2, U5, PackedU128>::MAX_INT, 31u128);
        assert_eq!(UintBitfield::<U2, U6, PackedU128>::MAX_INT, 63u128);
        assert_eq!(UintBitfield::<U31, U15, PackedU128>::MAX_INT, 32767u128);
        assert_eq!(UintBitfield::<U0, U128, PackedU128>::MAX_INT, u128::MAX);

        assert_eq!(
            UintBitfield::<U0, U1, PackedU128>::new(1u128).unwrap(),
            UintBitfield::<U0, U1, PackedU128>::MAX
        );
        assert_eq!(
            UintBitfield::<U3, U4, PackedU128>::new(15u128).unwrap(),
            UintBitfield::<U3, U4, PackedU128>::MAX
        );
        assert_eq!(
            UintBitfield::<U2, U6, PackedU128>::new(63u128).unwrap(),
            UintBitfield::<U2, U6, PackedU128>::MAX
        );
        assert_eq!(
            UintBitfield::<U31, U15, PackedU128>::new(32767u128).unwrap(),
            UintBitfield::<U31, U15, PackedU128>::MAX
        );
        assert_eq!(
            UintBitfield::<U0, U128, PackedU128>::new(u128::MAX).unwrap(),
            UintBitfield::<U0, U128, PackedU128>::MAX
        );
    }

    #[test]
    /// It should be possible to create a const UintBitfield for a PackedU8 packed
    /// type using `const_new`.
    fn test_const_new_u8() {

        const TEST_BITFIELD: UintBitfield<U0, U4, PackedU8> =
            match UintBitfield::<U0, U4, PackedU8>::const_new(5) {
                Ok(bf) => bf,
                Err(_) => panic!("Invalid value for constructing bitfield")
            };

        assert_eq!(TEST_BITFIELD, UintBitfield::<U0, U4, PackedU8>::new(5).unwrap());

        // Check it tests the values properly
        let _rt_val = UintBitfield::<U0, U4, PackedU8>::const_new(15).unwrap();
        assert!(UintBitfield::<U0, U4, PackedU8>::const_new(16).is_err())
    }

    #[test]
    /// It should be possible to create a const UintBitfield for a PackedU16 packed
    /// type using `const_new`.
    fn test_const_new_u16() {

        const TEST_BITFIELD: UintBitfield<U0, U4, PackedU16> =
            match UintBitfield::<U0, U4, PackedU16>::const_new(5) {
                Ok(bf) => bf,
                Err(_) => panic!("Invalid value for constructing bitfield")
            };

        assert_eq!(TEST_BITFIELD, UintBitfield::<U0, U4, PackedU16>::new(5).unwrap());

        // Check it tests the values properly
        let _rt_val = UintBitfield::<U0, U4, PackedU16>::const_new(15).unwrap();
        assert!(UintBitfield::<U0, U4, PackedU16>::const_new(16).is_err())
    }

    #[test]
    /// It should be possible to create a const UintBitfield for a PackedU32 packed
    /// type using `const_new`.
    fn test_const_new_u32() {

        const TEST_BITFIELD: UintBitfield<U0, U4, PackedU32> =
            match UintBitfield::<U0, U4, PackedU32>::const_new(5) {
                Ok(bf) => bf,
                Err(_) => panic!("Invalid value for constructing bitfield")
            };

        assert_eq!(TEST_BITFIELD, UintBitfield::<U0, U4, PackedU32>::new(5).unwrap());

        // Check it tests the values properly
        let _rt_val = UintBitfield::<U0, U4, PackedU32>::const_new(15).unwrap();
        assert!(UintBitfield::<U0, U4, PackedU32>::const_new(16).is_err())
    }

    #[test]
    /// It should be possible to create a const UintBitfield for a PackedU64 packed
    /// type using `const_new`.
    fn test_const_new_u64() {

        const TEST_BITFIELD: UintBitfield<U0, U4, PackedU64> =
            match UintBitfield::<U0, U4, PackedU64>::const_new(5) {
                Ok(bf) => bf,
                Err(_) => panic!("Invalid value for constructing bitfield")
            };

        assert_eq!(TEST_BITFIELD, UintBitfield::<U0, U4, PackedU64>::new(5).unwrap());

        // Check it tests the values properly
        let _rt_val = UintBitfield::<U0, U4, PackedU64>::const_new(15).unwrap();
        assert!(UintBitfield::<U0, U4, PackedU64>::const_new(16).is_err())
    }

    #[test]
    /// It should be possible to create a const UintBitfield for a PackedU128 packed
    /// type using `const_new`.
    fn test_const_new_u128() {

        const TEST_BITFIELD: UintBitfield<U0, U4, PackedU128> =
            match UintBitfield::<U0, U4, PackedU128>::const_new(5) {
                Ok(bf) => bf,
                Err(_) => panic!("Invalid value for constructing bitfield")
            };

        assert_eq!(TEST_BITFIELD, UintBitfield::<U0, U4, PackedU128>::new(5).unwrap());

        // Check it tests the values properly
        let _rt_val = UintBitfield::<U0, U4, PackedU128>::const_new(15).unwrap();
        assert!(UintBitfield::<U0, U4, PackedU128>::const_new(16).is_err())
    }
}

#[cfg(test)]
mod const_hi_bitfield_tests {
    //! The boolean bitfield should contain everything needed to pack
    //! a boolean value into an integer with the default 32-bit output.
    use crate::PackedU16;

    use super::{Bitfield, ConstHiBitfield, BitfieldConfig, BitfieldError};
    use typenum::{Unsigned, consts::*};

    #[test]
    /// The ConstHiBitfield should pack a true into the correct location
    fn test_pack() {
        // We test a few cases, including the limits
        let bf = ConstHiBitfield::<U0>::new();
        assert_eq!(bf.pack().unwrap(), 1 << 0);

        let bf = ConstHiBitfield::<U1>::new();
        assert_eq!(bf.pack().unwrap(), 1 << 1);

        let bf = ConstHiBitfield::<U10>::new();
        assert_eq!(bf.pack().unwrap(), 1 << 10);

        let bf = ConstHiBitfield::<U1, PackedU16>::new();
        assert_eq!(bf.pack().unwrap(), 1u16 << 1);

        let bf = ConstHiBitfield::<U10, PackedU16>::new();
        assert_eq!(bf.pack().unwrap(), 1u16 << 10);
    }

    #[test]
    /// The ConstHiBitfield should implement the Into<bool> trait, supporting
    /// exporting its internal value.
    fn test_into() {
        // We test a few cases, including the limits
        let bf = ConstHiBitfield::<U0>::new();
        let into_val: bool = bf.into();
        assert_eq!(into_val, true);

        let bf = ConstHiBitfield::<U14, PackedU16>::new();
        let into_val: bool = bf.into();
        assert_eq!(into_val, true);
    }

    #[test]
    /// The ConstHiBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct constants as specified by the trait.
    fn test_bitfield_config_consts() {
        // Length is always 1.
        assert_eq!(ConstHiBitfield::<U0, PackedU16>::LENGTH, 1usize);
        assert_eq!(ConstHiBitfield::<U10, PackedU16>::LENGTH, 1usize);
        assert_eq!(ConstHiBitfield::<U6>::LENGTH, 1usize);
        assert_eq!(ConstHiBitfield::<U20>::LENGTH, 1usize);

        assert_eq!(ConstHiBitfield::<U0, PackedU16>::OFFSET, 0usize);
        assert_eq!(ConstHiBitfield::<U10, PackedU16>::OFFSET, 10usize);
        assert_eq!(ConstHiBitfield::<U6>::OFFSET, 6usize);
        assert_eq!(ConstHiBitfield::<U20>::OFFSET, 20usize);
    }

    #[test]
    /// The ConstHiBitfield should properly implement the BitfieldConfig trait,
    /// providing the correct associated types as specified by the trait.
    fn test_bitfield_config_associated_types() {
        // Length is always 1.
        assert_eq!(
            <ConstHiBitfield::<U0, PackedU16> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <ConstHiBitfield::<U10, PackedU16> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <ConstHiBitfield::<U6> as BitfieldConfig>::Length::USIZE,
            1usize
        );
        assert_eq!(
            <ConstHiBitfield::<U20> as BitfieldConfig>::Length::USIZE,
            1usize
        );

        assert_eq!(
            <ConstHiBitfield::<U0, PackedU16> as BitfieldConfig>::Offset::USIZE,
            0usize
        );
        assert_eq!(
            <ConstHiBitfield::<U10, PackedU16> as BitfieldConfig>::Offset::USIZE,
            10usize
        );
        assert_eq!(
            <ConstHiBitfield::<U6> as BitfieldConfig>::Offset::USIZE,
            6usize
        );
        assert_eq!(
            <ConstHiBitfield::<U20> as BitfieldConfig>::Offset::USIZE,
            20usize
        );
    }

    #[test]
    /// The ConstHiBitfield should properly implement the BitfieldConfig trait
    /// which provides a method to lookup the associated constant values
    /// on instances of the bitfield.
    fn test_bitfield_config_methods() {
        // Length is always 1.
        let bitfield = ConstHiBitfield::<U0, PackedU16>::new();
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 0usize);

        let bitfield = ConstHiBitfield::<U10, PackedU16>::new();
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 10usize);

        let bitfield = ConstHiBitfield::<U6>::new();
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 6usize);

        let bitfield = ConstHiBitfield::<U20>::new();
        assert_eq!(bitfield.length(), 1usize);
        assert_eq!(bitfield.offset(), 20usize);
    }

    #[test]
    /// The ConstHiBitfield should implement into_inner, supporting
    /// exporting its internal value.
    fn test_into_inner() {
        // We test a few cases, including the limits
        let bf = ConstHiBitfield::<U0>::new();
        // Until type ascription is stable, we need an intermediate variable
        // (type ascription would be `bf.into_inner(): u32`)`
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

        let bf = ConstHiBitfield::<U10, PackedU16>::new();
        let into_val: bool = bf.into_inner();
        assert_eq!(into_val, true);

    }

    #[test]
    /// Unpacking a value should be supported with the `Bitfield::unpack` method.
    /// If the value being unpacked is high, then it should return Ok with a
    /// `ConstHiBitfield`. If the value being unpacked is low, then an error
    /// should be returned.
    fn test_unpacking() {
        // Fine
        let val = 1u16 << 15;
        let bf = ConstHiBitfield::<U15, PackedU16>::new();
        assert_eq!(ConstHiBitfield::<U15, PackedU16>::unpack(val).unwrap(), bf);

        // Error
        let val = 0u16 << 15;
        assert!(matches!(
                ConstHiBitfield::<U15, PackedU16>::unpack(val),
                Err(BitfieldError::InvalidConstHiUnpackedValue)
        ));
    }


    #[test]
    /// The ConstHiBitfield should implement the Default trait.
    fn test_default() {
        let bf = ConstHiBitfield::<U21>::default();
        assert_eq!(Into::<bool>::into(bf), true);
        assert_eq!(bf.pack().unwrap(), 1u32 << 21);
    }
}

