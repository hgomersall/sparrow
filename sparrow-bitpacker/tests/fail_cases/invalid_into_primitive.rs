use typenum::consts::*;

use sparrow_bitpacker::{UintBitfield, PackedU32};

fn main() -> () {

    // Should work
    let a: UintBitfield::<U0, U16, PackedU32, U0> = Default::default();
    let _: u16 = a.into();

    // Should cause compile fail because 17 bits don't fit into a u16, so
    // the trait is not implemented.
    let b: UintBitfield::<U0, U17, PackedU32, U0> = Default::default();
    let _: u16 = b.into();
}
