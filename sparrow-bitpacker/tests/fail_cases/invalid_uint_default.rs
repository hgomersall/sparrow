use typenum::consts::*;

use sparrow_bitpacker::{UintBitfield, PackedU8};

fn main() -> () {

    // Should work
    let _a: UintBitfield::<U0, U6, PackedU8, U63> = Default::default();

    // Should cause compile fail
    let _b: UintBitfield::<U0, U6, PackedU8, U64> = Default::default();
}
